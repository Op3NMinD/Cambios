﻿namespace Cambios
{
    using Modelos;
    using Servicos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Threading.Tasks;
    using System;

    public partial class Form1 : Form
    {
        #region Atributos

        private List<Rate> Rates;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;

        #endregion

        

        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            LoadRates();
        }

        private async void LoadRates()
        {

            bool Load;

            //ProgressBar1.Value = 0;

            LabelResultado.Text = "A atualizar taxas...";

            var connection = networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                LoadLocalRates();
                Load = false;
            }
            else
            {
                await LoadApiRates();
                Load = true;
            }

            if(Rates.Count == 0)
            {
                LabelResultado.Text = "Não há ligação à internet e não foram previamente carregadas as taxas /n Tente mais tarde";

                LabelStatus.Text = "Primeira inicialização deverá ter ligação à internet";

                return;
            }


            ComboBoxOrigem.DataSource = Rates;
            ComboBoxOrigem.DisplayMember = "Name";

            //corrige bug da microsoft 
            ComboBoxDestino.BindingContext = new BindingContext();

            ComboBoxDestino.DataSource = Rates;
            ComboBoxDestino.DisplayMember = "Name";              

            LabelResultado.Text = "Taxas atualizadas...";

            if (Load)
            {
                LabelStatus.Text = string.Format("Taxas Carregadas da Internet em {0:F}", DateTime.Now);
            }
            else
            {
                LabelStatus.Text = string.Format("Taxas carregadas da Base de Dados");
            }

            ProgressBar1.Value = 100;

            ButtonConverter.Enabled = true;
            ButtonTroca.Enabled = true;
        }

        private void LoadLocalRates()
        {
            Rates = dataService.GetData();
        }

        private async Task LoadApiRates()
        {
            ProgressBar1.Value = 0;

            var response = await apiService.GetRates("http://apiexchangerates.azurewebsites.net","/api/rates");

            Rates = (List<Rate>)response.Result;

            dataService.DeleteData();

            dataService.SaveData(Rates);

        }

        private void ButtonConverter_Click(object sender, EventArgs e)
        {
            Converter();
        }

        private void Converter()
        {
            if (string.IsNullOrEmpty(TextBoxValor.Text))
            {
                dialogService.ShowMessage("Erro","Insira um valor a converter");
                return;
            }

            decimal valor;
            if(!decimal.TryParse(TextBoxValor.Text, out valor))
            {
                dialogService.ShowMessage("Erro de conversão", "Valor terá que ser numérico");
                return;
            }

            if(ComboBoxOrigem.SelectedItem == null)
            {
                dialogService.ShowMessage("Erro", "Tem que escolher uma moeda de Origem");
                return;
            }

            if (ComboBoxDestino.SelectedItem == null)
            {
                dialogService.ShowMessage("Erro", "Tem que escolher uma moeda de Destino");
                return;
            }

            var taxaOrigem = (Rate) ComboBoxOrigem.SelectedItem;
            var taxaDestino = (Rate)ComboBoxDestino.SelectedItem;

            var valorConvertido = valor / (decimal)taxaOrigem.TaxRate * (decimal)taxaDestino.TaxRate;

            LabelResultado.Text = string.Format("{0:N2} {1} = {2:N2} {3}", valor, taxaOrigem.Code, valorConvertido, taxaDestino.Code);
        }

        private void ButtonTroca_Click(object sender, EventArgs e)
        {
            Troca();
        }

        private void Troca()
        {
            var aux = ComboBoxOrigem.SelectedItem;
            ComboBoxOrigem.SelectedItem = ComboBoxDestino.SelectedItem;
            ComboBoxDestino.SelectedItem = aux;
            Converter();
        }


    }
}
